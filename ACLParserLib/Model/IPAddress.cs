﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACLParserLib.Model
{
    public class IPAddress
    {
        public IPAddress(string ip)
        {
            if (ip == "*")
                return;

            var parts = ip.Split('.');

            FirstOct = parts[0];
            SecondOct = parts[1];
            ThirdOct = parts[2];
            FourthOct = parts[3];
        }

        public string FirstOct { get; set; }
        public string SecondOct { get; set; }
        public string ThirdOct { get; set; }
        public string FourthOct { get; set; }


        public static bool IsValidIP(string ip)
        {
            if (string.IsNullOrEmpty(ip))
                return false;

            if (ip == "*")
                return true;

            var parts = ip.Split('.');

            if (parts.Length != 4)
                return false;

            foreach (var part in parts)
            {
                byte _;
                if (part != "*" && !byte.TryParse(part, out _))
                    return false;
            }

            return true;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(FirstOct) &&
                string.IsNullOrEmpty(SecondOct) &&
                string.IsNullOrEmpty(ThirdOct) &&
                string.IsNullOrEmpty(FourthOct))
                return "*";
            return $"{FirstOct}.{SecondOct}.{ThirdOct}.{FourthOct}";
        }
    }
}
