﻿using ACLParserLib.Engine;
using ACLParserLib.Model;
using ACLParserLib.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(ConfigFileExtension = "log4net", Watch = true)]

namespace ACLParserLibTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start running tests for ACLParserLib");
            
            TestSingleRule();
            TestMultiRules();
            TestMultiRulesParallel();
            TestMultiRulesConcurrentWithErrors();
            TestMultiRulesConcurrent();

            Console.WriteLine("Click enter key to exit");
            Console.Read();
        }
        private static void TestMultiRulesConcurrent()
        {
            Console.WriteLine("Test multi rules in parallel");

            ACLRulesStore store = new ACLRulesStore();
            List<string> rules = new List<string>();

            for (int i = 0; i < 100; i++)
            {
                var action = (ACLAction)(i % 2);
                var protocol = (ACLProtocol)(i % 3);
                Random r = new Random(i);
                string sourceIP = $"{r.Next() % 256}.{r.Next() % 256}.{r.Next() % 256}.{r.Next() % 256}";
                string sourcePort = (r.Next() % ushort.MaxValue).ToString();
                string destIP = $"{r.Next() % 256}.{r.Next() % 256}.{r.Next() % 256}.{r.Next() % 256}";
                string destPort = (r.Next() % ushort.MaxValue).ToString();
                rules.Add($"R_ACL{i} {action} {protocol} {sourceIP} {sourcePort} {destIP} {destPort}");
            }


            Task<ACLRule>[] results = new Task<ACLRule>[rules.Count()];
            int j = 0;
            foreach (var rule in rules)
            {
                results[j++] = Task.Factory.StartNew(() => Parser.Parse(rule).First().Result);
            }
            Task.WaitAll(results);


            store.AddMany(results.Select(t => t.Result));
            Console.WriteLine($"Store contains these rules:\n {store.ToString()}");
        }

        private static void TestMultiRulesConcurrentWithErrors()
        {
            Console.WriteLine("Test multi rules in parallel");

            ACLRulesStore store = new ACLRulesStore();
            List<string> rules = new List<string>();
            rules.Add("R_ACL1 permit tcp 192.168.0.* 8080");
            rules.Add("R_ACL2 deny tcp 192.168.       *.* 80 192.168.*.* 80");
            rules.Add("R_ACL3 deny any 192.168.0.254 21 1     92.168.0.100 21");
            rules.Add("R_ACL4 permit icmp * ***");


            Task<ACLRule>[] results = new Task<ACLRule>[rules.Count()];
            int i = 0;
            foreach (var rule in rules)
            {
                results[i++] = Task.Factory.StartNew(() => Parser.Parse(rule).First().Result);
            }
            Task.WaitAll(results);


            store.AddMany(results.Select(t => t.Result));
            Console.WriteLine($"Store contains these rules:\n {store.ToString()}");
        }

        private static void TestMultiRulesParallel()
        {
            Console.WriteLine("Test multi rules in parallel");

            ACLRulesStore store = new ACLRulesStore();
            List<string> rules = new List<string>();
            rules.Add("R_ACL1 permit tcp 192.168.0.125 8080 192.168.0.* 8080");
            rules.Add("R_ACL2 deny tcp 192.168.*.* 80 192.168.*.* 80");
            rules.Add("R_ACL3 deny any 192.168.0.254 21 192.168.0.100 21");
            rules.Add("R_ACL4 permit icmp * * * *");


            Task<ACLRule>[] results = new Task<ACLRule>[rules.Count()];
            int i = 0;
            foreach (var rule in rules)
            {
                results[i++] = Task.Factory.StartNew(() => Parser.Parse(rule).First().Result);
            }
            Task.WaitAll(results);


            store.AddMany(results.Select(t => t.Result));
            Console.WriteLine($"Store contains these rules:\n {store.ToString()}");
        }

        private static void TestMultiRules()
        {
            Console.WriteLine("Test multi rules");

            ACLRulesStore store = new ACLRulesStore();


            string acl = "R_ACL1 permit tcp 192.168.0.125 8080 192.168.0.* 8080\n"
                        +"R_ACL2 deny tcp 192.168.*.* 80 192.168.*.* 80\n"
                        +"R_ACL3 deny any 192.168.0.254 21 192.168.0.100 21\n"
                        +"R_ACL4 permit icmp * * * *";

            store.AddMany(Parser.Parse(acl).Select(t => t.Result));

            Console.WriteLine($"Store contains these rules:\n {store.ToString()}");
        }

        private static void TestSingleRule()
        {
            Console.WriteLine("Test singele rule");

            ACLRulesStore store = new ACLRulesStore();

            string acl = "R_ACL1 permit tcp 192.168.0.125 8080 192.168.0.* 8080";

            store.AddMany(Parser.Parse(acl).Select(t => t.Result));

            Console.WriteLine($"Store contains these rules:\n {store.ToString()}");
        }
    }
}
