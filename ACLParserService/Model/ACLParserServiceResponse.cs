﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACLParserService.Model
{
    public class ACLParserServiceResponse
    {
        public bool IsSuccess { get { return string.IsNullOrEmpty(Error); } }
        public string Error { get; set; }
    }
}
