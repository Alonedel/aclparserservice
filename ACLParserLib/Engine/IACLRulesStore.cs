﻿using ACLParserLib.Model;
using System.Collections.Generic;

namespace ACLParserLib.Engine
{
    public interface IACLRulesStore
    {
        public bool Add(ACLRule rule);
        public void AddMany(IEnumerable<ACLRule> rules);

        public bool Remove(ACLRule rule);

        public bool Remove(string name);

        public IEnumerable<ACLRule> List();
    }
}