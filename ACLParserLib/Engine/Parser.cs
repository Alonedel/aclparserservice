﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ACLParserLib.Model;

namespace ACLParserLib.Parser
{
    public class Parser
    {
        #region log4net
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion //log4net

        public static IEnumerable<Task<ACLRule>> Parse(string aclsRules)
        {
            if (string.IsNullOrEmpty(aclsRules))
                return new Task<ACLRule>[0];

            var lines = aclsRules.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            Task<ACLRule>[] results = new Task<ACLRule>[lines.Count()];
            StringBuilder errors = new StringBuilder();
            try
            {
                int i = 0;
                foreach (var line in lines)
                {
                    results[i++] = Task.Factory.StartNew(() => ParseSingleLine(line));
                }
                Task.WaitAll(results);
            }
            catch(Exception ex)
            {
                log.Error($"Failed to Parse ACL rules. exception message: {ex.Message}");
            }

            return results;
        }

        private static ACLRule ParseSingleLine(string line)
        {
            ACLRule ret = new ACLRule();
            try
            {
                int length = line.Length;
                int i = 0;
                char c;

                //Parse name
                StringBuilder sbName = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbName.Append(c);
                }
                ret.Name = sbName.ToString();

                //Parse Action
                StringBuilder sbAction = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbAction.Append(c);
                }
                ret.Action = ParseAction(sbAction.ToString());

                //Parse Protocol
                StringBuilder sbProtocol = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbProtocol.Append(c);
                }
                ret.Protocol = ParseProtocol(sbProtocol.ToString());

                //Parse SourceIP
                StringBuilder sbSourceIP = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbSourceIP.Append(c);
                }
                ret.SourceIP = ParseIP(sbSourceIP.ToString());

                //Parse SourcePort
                StringBuilder sbSourcePort = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbSourcePort.Append(c);
                }
                ret.SourcePort = ParsePort(sbSourcePort.ToString());

                //Parse DestIP
                StringBuilder sbDestIP = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbDestIP.Append(c);
                }
                ret.DestIP = ParseIP(sbDestIP.ToString());

                //Parse SourcePort
                StringBuilder sbDestPort = new StringBuilder();
                while (i < length && (c = line[i++]) != ' ')
                {
                    sbDestPort.Append(c);
                }
                ret.DestPort = ParsePort(sbDestPort.ToString());
            }
            catch (Exception ex)
            {
                ret.Err = $"Failed to Parse ACL rule. Exception message: {ex.Message}";
            }

            return ret;
        }

        private static Port ParsePort(string port)
        {
            Port ret;
            if(!Port.TryParsePort(port, out ret))
                throw new FormatException("Invalid Port format or value");

            return ret;
        }

        private static IPAddress ParseIP(string ip)
        {           
            if(!IPAddress.IsValidIP(ip))
                throw new FormatException("Invalid IP format");
            
            return new IPAddress(ip);
        }

        private static ACLProtocol ParseProtocol(string protocol)
        {
            return (ACLProtocol)Enum.Parse(typeof(ACLProtocol), protocol, true);
        }

        private static ACLAction ParseAction(string action)
        {
            return (ACLAction)Enum.Parse(typeof(ACLAction), action,true);
        }
    }
}
