﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACLParserLib.Model
{
    public enum ACLProtocol
    {
        TCP = 0,
        UDP,
        ICMP,
        Any
    }
}
