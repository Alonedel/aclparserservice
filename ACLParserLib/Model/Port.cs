﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACLParserLib.Model
{
    public class Port
    {
        public ushort Val { get; private set; }
        public bool WildCard { get; private set; }

        public static bool TryParsePort(string sPort,out Port port)
        {
            port = new Port();
            if (sPort == "*")
            {
                port.WildCard = true;
                return true;
            }

            ushort val;
            if (!ushort.TryParse(sPort, out val))
                return false;

            port.Val = val;
            return true;
        }

        public override string ToString()
        {
            if (WildCard)
                return "*";
            return Val.ToString();
        }
    }
}
