This Project includes 3 projects:
1) ACLParserLib - Includes the Parser and the model of an ACLRule.
2) ACLParserService - RestAPI which allow to add rules by POST 'https://<DNS>/ACLParser?rules=<rules string>'
	You may run this locally (maybe the port will change) 'https://localhost:44371/ACLParser?rules=R_ACL5 permit tcp 192.168.0.125 * 192.168.0.* *' with postman 
	or use CURL: curl --location --request POST "https://localhost:44371/ACLParser?rules=R_ACL5%20permit%20tcp%20192.168.0.125%20*%20192.168.0.*%20*" --data-raw ''
3) ACLParserLibTests - Console application which includes several tests on the BL sequential and in parallel.

Valid input:
Parser can get as input a string with single/multi lines.
Each line expected to be in the following format: '<name> <action> <protocol> <sourcip> <sourceport> <destip> <sedtport>'
name - can be anything
action - permit or deny (ignore case)
protocol -  TCP,UDP,ICMP,Any (ignore case)
sourcip/destip - type of IPAddress which includes 4 octs that can get values of 0-255 or *.
sourceport/destport - type of Port and can get values of 0-65535(ushort.Max) or *.  

Any vaiolation of the expected format will result an invalid ACLRule.

