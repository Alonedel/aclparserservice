﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACLParserLib.Model
{
    public enum ACLAction
    {
        Deny = 0,
        Permit
    }
}
