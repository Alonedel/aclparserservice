﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACLParserLib.Engine;
using ACLParserLib.Model;
using ACLParserLib.Parser;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ACLParserService.Model;
using System.Text;

namespace ACLParserService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ACLParserController : ControllerBase
    {
        private readonly ILogger<ACLParserController> _logger;

        public ACLParserController(ILogger<ACLParserController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<ACLRule>> Get()
        {
            IEnumerable<ACLRule> ret = await Task.Factory.StartNew(() =>
            {
                var services = this.HttpContext.RequestServices;
                var store = (IACLRulesStore)services.GetService(typeof(IACLRulesStore));
                return store.List();
            });
            return ret;
        }

        [HttpPost]
        public async Task<ACLParserServiceResponse> Post(string rules)
        {
            ACLParserServiceResponse ret = await Task.Factory.StartNew(() =>
            {
                var services = this.HttpContext.RequestServices;
                var store = (IACLRulesStore)services.GetService(typeof(IACLRulesStore));

                ACLParserServiceResponse res = new ACLParserServiceResponse();

                try
                {
                    store.AddMany(Parser.Parse(rules).Select(t => t.Result));
                }
                catch (Exception ex)
                {
                    res.Error = ex.Message;
                }
                return res;
            });

            return ret;

        }
    }
}
