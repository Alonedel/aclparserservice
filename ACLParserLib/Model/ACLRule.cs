using System;
using System.Linq;
using System.Net;
using System.Text;

namespace ACLParserLib.Model
{
    public class ACLRule
    {
        public string Name { get; set; }

        public ACLAction Action { get; set; }

        public ACLProtocol Protocol { get; set; }

        public IPAddress SourceIP { get; set; }

        public Port SourcePort { get; set; }

        public IPAddress DestIP { get; set; }

        public Port DestPort { get; set; }

        public bool IsValid { get { return string.IsNullOrEmpty(Err); } }
        public string Err { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var props = typeof(ACLRule).GetProperties();

            foreach (var p in props)
            {
                sb.Append($"{p.Name}: {p.GetValue(this)} ");  
            }

            return sb.ToString();
        }

    }

}
