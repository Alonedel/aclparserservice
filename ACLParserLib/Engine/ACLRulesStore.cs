﻿using ACLParserLib.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ACLParserLib.Engine
{
    public class ACLRulesStore : IACLRulesStore
    {
        ConcurrentDictionary<string, ACLRule> ACLRules;

        public ACLRulesStore()
        {
            ACLRules = new ConcurrentDictionary<string, ACLRule>();
        }

        public bool Add(ACLRule rule)
        {
            return ACLRules.TryAdd(rule.Name,rule);
        }

        public void AddMany(IEnumerable<ACLRule> rules) 
        {
            rules.AsParallel().ForAll(r => Add(r));
        }

        public bool Remove(ACLRule rule)
        {
            return Remove(rule.Name);
        }

        public bool Remove(string name)
        {
            ACLRule removedRule;
            return ACLRules.TryRemove(name, out removedRule);
        }

        public IEnumerable<ACLRule> List()
        {
            return ACLRules.Values;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var v in ACLRules.Values)
            {
                sb.AppendLine(v.ToString());
            }
            return sb.ToString();

        }
    }
}
